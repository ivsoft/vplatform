﻿using System;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Utils
{
    public interface IXmlImportContext
    {
        T Get<T>();
        void PostImportAction(Action<IXmlImportContext> action);
        void PostImportAction(Func<IXmlImportContext, Task> action);
    }
}
