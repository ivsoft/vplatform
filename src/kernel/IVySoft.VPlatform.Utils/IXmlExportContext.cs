﻿using System;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Utils
{
    public interface IXmlExportContext
    {
        T Get<T>();
        void PostExportAction(Action<IXmlExportContext> action);
        void PostExportAction(Func<IXmlExportContext, Task> action);
    }
}
