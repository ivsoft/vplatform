﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Utils
{
    public class XmlExportContext : IXmlExportContext
    {
        private Dictionary<Type, object> _properties = new Dictionary<Type, object>();
        public List<Func<IXmlExportContext, Task>> PostActions = new List<Func<IXmlExportContext, Task>>();
        public T Get<T>()
        {
            object result;
            if(!_properties.TryGetValue(typeof(T), out result))
            {
                return default;
            }
            return (T)result;
        }
        public void Set<T>(T value)
        {
            _properties[typeof(T)] = value;
        }

        public void PostExportAction(Func<IXmlExportContext, Task> action)
        {
            PostActions.Add(action);
        }
        public void PostExportAction(Action<IXmlExportContext> action)
        {
            PostActions.Add((context) => { action(context); return Task.CompletedTask; });
        }
    }
}
