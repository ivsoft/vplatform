﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;

namespace IVySoft.VPlatform.Utils
{
    public static class CSharpUtils
    {
        public static string TypeShortName(string type_name)
        {
            switch (type_name)
            {
                case "System.Boolean": return "bool";
                case "System.String": return "string";
                case "System.Int32": return "int";
                case "System.DateTime": return "System.DateTime";
                case "System.Int64": return "long";
                default: return type_name;
            }
        }
        public static string FormatCode(string soureCode)
        {
            var tree = CSharpSyntaxTree.ParseText(soureCode);
            var root = (CSharpSyntaxNode)tree.GetRoot();
            return root.NormalizeWhitespace().ToFullString();
        }
    }
}
