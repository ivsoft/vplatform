﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Utils
{
    public class XmlImportContext : IXmlImportContext
    {
        private Dictionary<Type, object> _properties = new Dictionary<Type, object>();
        public List<Func<IXmlImportContext, Task>> PostActions = new List<Func<IXmlImportContext, Task>>();
        public T Get<T>()
        {
            object result;
            if (!_properties.TryGetValue(typeof(T), out result))
            {
                return default;
            }
            return (T)result;
        }
        public void Set<T>(T value)
        {
            _properties[typeof(T)] = value;
        }

        public void PostImportAction(Action<IXmlImportContext> action)
        {
            PostActions.Add((context) => { action(context); return Task.CompletedTask; });
        }
        public void PostImportAction(Func<IXmlImportContext, Task> action)
        {
            PostActions.Add(action);
        }
    }
}
