﻿using IVySoft.VPlatform.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVySoft.VPlatform.Mails
{
    public interface IDbModel : Model.IDbModel
    {
        DbSet<MailMessage> Mails { get; set; }
    }
}
