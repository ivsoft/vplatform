using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IVySoft.VPlatform.Mails
{
    public class MailMessage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int SentCount { get; set; }
        public System.DateTime LastSent { get; set; }
        public string Error { get; set; }

    }
}
