﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace IVySoft.VPlatform.Model
{
    public class RuntimeContext
    {
        private readonly IServiceProvider _sp;
        private readonly CancellationToken _ct;
        private readonly IPrincipal _principal;

        public RuntimeContext(IServiceProvider sp, CancellationToken token, IPrincipal principal)
        {
            _sp = sp;
            _ct = token;
            _principal = principal;
        }

        public static RuntimeContext _default;
        public static AsyncLocal<RuntimeContext> _current = new AsyncLocal<RuntimeContext>();

        public static RuntimeContext Default
        {
            get
            {
                return _default;
            }
            set
            {
                _default = value;
            }
        }
        public static RuntimeContext GetCurrent(bool allowDefault)
        {
            if (null == _current.Value && allowDefault)
                return _default;
            else
                return _current.Value;
        }
        internal static RuntimeContext Current
        {
            get
            {
                return _current.Value ?? _default;
            }
            set
            {
                _current.Value = value;
            }
        }

        public static IServiceProvider ServiceProvider
        {
            get
            {
                return Current._sp;
            }
        }

        public static CancellationToken CancellationToken
        {
            get
            {
                return Current._ct;
            }
        }
        public static IPrincipal Principal
        {
            get
            {
                return Current._principal;
            }
        }
    }
}
