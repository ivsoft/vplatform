﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Model
{
    public class RuntimeContextScope : IDisposable, IAsyncDisposable
    {
        private readonly RuntimeContext _prev;
        public RuntimeContextScope(IServiceProvider sp, CancellationToken token, IPrincipal principal)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(sp, token, principal);
        }
        public RuntimeContextScope(IServiceProvider sp, CancellationToken token)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(sp, token, RuntimeContext.Principal);
        }
        public RuntimeContextScope(IServiceProvider sp, IPrincipal principal)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(sp, RuntimeContext.CancellationToken, principal);
        }
        public RuntimeContextScope(IServiceProvider sp)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(sp, RuntimeContext.CancellationToken, RuntimeContext.Principal);
        }
        public RuntimeContextScope(CancellationToken token)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(RuntimeContext.ServiceProvider, token, RuntimeContext.Principal);
        }
        public RuntimeContextScope(IPrincipal principal)
        {
            _prev = RuntimeContext.GetCurrent(false);
            RuntimeContext.Current = new RuntimeContext(RuntimeContext.ServiceProvider, RuntimeContext.CancellationToken, principal);
        }

        public void Dispose()
        {
            RuntimeContext.Current = _prev;
        }

        public ValueTask DisposeAsync()
        {
            RuntimeContext.Current = _prev;
            return default;
        }
    }
}
