﻿using System;

namespace IVySoft.VPlatform.Model
{
    public class NotAuthorizedException : Exception
    {
        public NotAuthorizedException(string message)
            : base(message)
        {
        }
        public NotAuthorizedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
