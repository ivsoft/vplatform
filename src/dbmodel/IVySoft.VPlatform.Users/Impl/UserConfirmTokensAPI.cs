using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVySoft.VPlatform.Users.Api;
using Microsoft.EntityFrameworkCore;

namespace IVySoft.VPlatform.Users.Impl
{
    public class UserConfirmTokensAPI : IUserConfirmTokensAPI
    {
        private readonly IDbModel _db;
        public UserConfirmTokensAPI(IDbModel db)
        {
            _db = db;
        }
        #region UserConfirmTokens

        public IQueryable<UserConfirmToken> Get(User user)
        {
            return _db.UserConfirmTokens;
        }
        public async ValueTask Add(User user, UserConfirmToken value)
        {
            await _db.UserConfirmTokens.AddAsync(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask Update(User user, UserConfirmToken update)
        {
            _db.Entry(update).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async ValueTask Delete(User user, UserConfirmToken value)
        {
            _db.UserConfirmTokens.Remove(value);
            await _db.SaveChangesAsync();
        }

        #endregion UserConfirmTokens
    }
}
