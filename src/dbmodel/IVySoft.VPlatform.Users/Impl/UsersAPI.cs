using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVySoft.VPlatform.Model;
using IVySoft.VPlatform.Users.Api;
using IVySoft.VPlatform.Users.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace IVySoft.VPlatform.Users.Impl
{
    public class UsersAPI : IUsersAPI
    {
        private readonly IDbModel _db;
        public UsersAPI(IDbModel db)
        {
            _db = db;
        }
        #region Users

        public IQueryable<User> Get()
        {
            return _db.Users;
        }
        public async ValueTask Add(User value)
        {
            await _db.Users.AddAsync(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask Update(User update)
        {
            _db.Entry(update).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async ValueTask Delete(User value)
        {
            _db.Users.Remove(value);
            await _db.SaveChangesAsync();
        }

        #endregion Users
        #region Tokens

        public IQueryable<UserConfirmToken> GetTokens(int Users_key)
        {
            return this.Get()
                .Where(x => x.Id == Users_key)
                .SelectMany(x => x.Tokens);
        }
        public async ValueTask AddToTokens(int Users_key, UserConfirmToken value)
        {
            var parent0 = this.Get().SingleOrDefault(x => x.Id == Users_key);
            if (null == parent0)
            {
                throw new NotFoundException($"Users {Users_key} was not found");
            }

            parent0.Tokens.Add(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask DeleteTokens(int Users_key, int key, UserConfirmToken value)
        {
            var parent0 = this.Get().SingleOrDefault(x => x.Id == Users_key);
            if (null == parent0)
            {
                throw new NotFoundException($"Users {Users_key} was not found");
            }
            var exist = parent0.Tokens.SingleOrDefault(x => x.Id == key);
            if (null == exist)
            {
                throw new NotFoundException($"UserConfirmToken {key} was not found");
            }

            parent0.Tokens.Remove(exist);
            await _db.SaveChangesAsync();
        }

        #endregion Tokens
        #region Action
        public async ValueTask<bool> Register(
            [FromServices] IOptions<ServerConfig> configuration,
            string email)
        {
            var user = this._db.Users.SingleOrDefault(x => x.Email.ToUpper() == email.ToUpper());
            if (null == user)
            {
                user = new User
                {
                    Name = email,
                    Email = email,
                    EmailConfirmed = false
                };

                await this._db.Users.AddAsync(user);
            }
            else if (user.EmailConfirmed)
            {
                return false;
            }

            var rnd = new Random();
            var confirmBytes = new byte[10];

            string confirmToken;
            for (; ; )
            {
                rnd.NextBytes(confirmBytes);
                confirmToken = Convert.ToBase64String(confirmBytes)
                .Replace("+", string.Empty)
                .Replace("/", string.Empty)
                .Replace("=", string.Empty);
                if (confirmToken.Length >= 10)
                {
                    confirmToken = confirmToken.Substring(0, 10);
                }

                if (!this._db.Users.Any(x => x.Tokens.Any(y => y.Token == confirmToken)))
                {
                    break;
                }
            }

            if (user.Tokens == null)
            {
                user.Tokens = new List<UserConfirmToken>();
            }

            user.Tokens.Add(new UserConfirmToken
            {
                Action = "Confirm",
                Created = DateTime.Now,
                Token = confirmToken
            });

            var mail = new Mails.MailMessage
            {
                To = email,
                Subject = "Confirm your email to join IVySoft Sitebuilder",
                Body = "Once you've confirmed your email address and set a password, you'll be the newest member of the IVySoft Sitebuilder.<br/>"
                + $"Confirm Email link <a href=\"{configuration.Value.PublicUrl}user/confirm/{confirmToken}\">{configuration.Value.PublicUrl}user/confirm/{confirmToken}</a><br/>"
                + $"Or input confirm code {confirmToken} in the from <a href=\"{configuration.Value.PublicUrl}user/confirm/\">{configuration.Value.PublicUrl}user/confirm/</a>",
            };

            await this._db.Mails.AddAsync(mail);
            await this._db.SaveChangesAsync();

            return true;
        }
        public async Task<string> Confirm(
            [FromServices] IOptions<JwtConfig> configuration,
            string token, string password)
        {
            var confirm = this._db.Users.SelectMany(x => x.Tokens.Where(y => y.Token == token && y.Action == "Confirm")).SingleOrDefault();
            if (null == confirm)
            {
                throw new NotFoundException("Token not found");
            }

            if (!confirm.TargetUser.EmailConfirmed)
            {
                confirm.TargetUser.EmailConfirmed = true;

                using (var sha256 = System.Security.Cryptography.SHA256.Create())
                {
                    confirm.TargetUser.Password = Convert.ToBase64String(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)));
                }
            }
            else
            {
                using (var sha256 = System.Security.Cryptography.SHA256.Create())
                {
                    if (confirm.TargetUser.Password != Convert.ToBase64String(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password))))
                    {
                        throw new NotFoundException("Invalid password");
                    }
                }
            }

            var claims = new List<System.Security.Claims.Claim>
            {
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, confirm.TargetUser.Email),
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, confirm.TargetUser.Id.ToString())
            };

            if (null != confirm.TargetUser.Name)
            {
                claims.Add(new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.GivenName, confirm.TargetUser.Name));
            }

            var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(configuration.Value.JwtKey));
            var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(30);

            var jwt_token = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(
                configuration.Value.JwtUser,
                configuration.Value.JwtAudience,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            await this._db.SaveChangesAsync();

            return new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler().WriteToken(jwt_token);
        }
        public async ValueTask<string> RefreshToken(
            [FromServices] IOptions<JwtConfig> configuration,
            [FromServices] User user)
        {
            var claims = new List<System.Security.Claims.Claim>
            {
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, user.Email),
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, user.Id.ToString())
            };

            if (null != user.Name)
            {
                claims.Add(new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.GivenName, user.Name));
            }

            var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(configuration.Value.JwtKey));
            var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(30);

            var jwt_token = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(
                configuration.Value.JwtUser,
                configuration.Value.JwtAudience,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler().WriteToken(jwt_token);
        }
        public async ValueTask<string> Login(
            [FromServices] IOptions<JwtConfig> configuration,
            string email, string password)
        {
            using (var sha256 = System.Security.Cryptography.SHA256.Create())
            {
                var user = this._db.Users.SingleOrDefault(x => x.Email == email
                && x.EmailConfirmed
                && x.Password == Convert.ToBase64String(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password))));
                if (null == user)
                {
                    throw new NotFoundException("User not found");
                }


                var claims = new List<System.Security.Claims.Claim>
            {
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, user.Email),
                new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, user.Id.ToString())
            };

                if (null != user.Name)
                {
                    claims.Add(new System.Security.Claims.Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.GivenName, user.Name));
                }


                var key = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(configuration.Value.JwtKey));
                var creds = new Microsoft.IdentityModel.Tokens.SigningCredentials(key, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256);
                var expires = DateTime.Now.AddMinutes(30);

                var jwt_token = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(
                    configuration.Value.JwtUser,
                    configuration.Value.JwtAudience,
                    claims,
                    expires: expires,
                    signingCredentials: creds
                );

                return new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler().WriteToken(jwt_token);
            }
        }

        #endregion Action
    }
}
