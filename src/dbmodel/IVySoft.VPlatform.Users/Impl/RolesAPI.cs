using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVySoft.VPlatform.Model;
using IVySoft.VPlatform.Users.Api;
using Microsoft.EntityFrameworkCore;

namespace IVySoft.VPlatform.Users.Impl
{
    public class RolesAPI : IRolesAPI
    {
        private readonly IDbModel _db;
        public RolesAPI(IDbModel db)
        {
            _db = db;
        }
        #region Roles

        public IQueryable<Role> Get(User user)
        {
            return _db.Roles;
        }
        public async ValueTask Add(User user, Role value)
        {
            await _db.Roles.AddAsync(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask Update(User user, Role update)
        {
            _db.Entry(update).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async ValueTask Delete(User user, Role value)
        {
            _db.Roles.Remove(value);
            await _db.SaveChangesAsync();
        }

        #endregion Roles
        #region Permissions

        public IQueryable<RolePermission> GetPermissions(User user, int Roles_key)
        {
            return this.Get(user)
                .Where(x => x.Id == Roles_key)

                .SelectMany(x => x.Permissions);
        }
        public async ValueTask AddToPermissions(User user, int Roles_key, RolePermission value)
        {
            var parent0 = this.Get(user).SingleOrDefault(x => x.Id == Roles_key);
            if (null == parent0)
            {
                throw new NotFoundException($"Roles {Roles_key} was not found");
            }

            parent0.Permissions.Add(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask DeletePermissions(User user, int Roles_key, int key, RolePermission value)
        {
            var parent0 = this.Get(user).SingleOrDefault(x => x.Id == Roles_key);
            if (null == parent0)
            {
                throw new NotFoundException($"Roles {Roles_key} was not found");
            }
            var exist = parent0.Permissions.SingleOrDefault(x => x.Id == key);
            if (null == exist)
            {
                throw new NotFoundException($"RolePermission {key} was not found");
            }

            parent0.Permissions.Remove(exist);
            await _db.SaveChangesAsync();
        }

        #endregion Permissions
    }
}
