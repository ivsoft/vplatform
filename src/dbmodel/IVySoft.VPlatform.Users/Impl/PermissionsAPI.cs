using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVySoft.VPlatform.Users.Api;
using Microsoft.EntityFrameworkCore;

namespace IVySoft.VPlatform.Users.Impl
{
    public class PermissionsAPI : IPermissionsAPI
    {
        private readonly IDbModel _db;
        public PermissionsAPI(IDbModel db)
        {
            _db = db;
        }
        #region Permissions

        public IQueryable<Permission> Get(User user)
        {
            return _db.Permissions;
        }
        public async ValueTask Add(User user, Permission value)
        {
            await _db.Permissions.AddAsync(value);
            await _db.SaveChangesAsync();
        }
        public async ValueTask Update(User user, Permission update)
        {
            _db.Entry(update).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async ValueTask Delete(User user, Permission value)
        {
            _db.Permissions.Remove(value);
            await _db.SaveChangesAsync();
        }

        #endregion Permissions
    }
}
