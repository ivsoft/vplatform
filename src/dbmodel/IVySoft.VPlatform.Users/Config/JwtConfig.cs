﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVySoft.VPlatform.Users.Config
{
    public class JwtConfig
    {
        public string JwtKey { get; set; }
        public string JwtUser { get; set; }
        public string JwtAudience { get; set; }
    }
}
