﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IVySoft.VPlatform.Users
{
    public interface IDbModel : Mails.IDbModel, Security.IDbModel
    {
        DbSet<User> Users { get; set; }
        DbSet<UserConfirmToken> UserConfirmTokens { get; set; }

        DbSet<Role> Roles { get; set; }
        DbSet<Permission> Permissions { get; set; }
    }
}
