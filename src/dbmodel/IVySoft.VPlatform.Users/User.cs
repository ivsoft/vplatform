using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IVySoft.VPlatform.Users
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        [Required]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Password { get; set; }
        public bool EmailConfirmed { get; set; }

        [InverseProperty(nameof(UserConfirmToken.TargetUser))]
        public virtual IList<UserConfirmToken> Tokens { get; set; }
    }
}
