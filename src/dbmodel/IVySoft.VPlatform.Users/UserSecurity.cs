﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVySoft.VPlatform.Users
{
    public static class UserSecurity
    {
        public static readonly string SystemObjectType = "System";

        public static readonly Role AdminRole = new Users.Role
        {
            Name = "Administrator",
            ObjectType = SystemObjectType
        };

        public static readonly Permission AdminPermission = new Users.Permission
        {
            Name = "Admin Permission",
            Description = "Allow to edit object security",
            Verb = "Admin",
        };

        public static readonly Permission EditPermission = new Users.Permission
        {
            Name = "Edit Permission",
            Description = "Allow to edit",
            Verb = "Edit",
        };

        public static readonly Permission DeletePermission = new Users.Permission
        {
            Name = "Delete Permission",
            Description = "Allow to delete",
            Verb = "Delete",
        };
    }
}
