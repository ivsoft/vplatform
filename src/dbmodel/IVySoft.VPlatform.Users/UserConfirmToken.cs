using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IVySoft.VPlatform.Users
{
    public class UserConfirmToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Token { get; set; }
        public string Action { get; set; }
        public DateTime Created { get; set; }

        [ForeignKey(nameof(TargetUser))]
        public int TargetUserId { get; set; }

        [InverseProperty(nameof(User.Tokens))]
        public virtual User TargetUser { get; set; }
    }
}
