using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Users.Api
{
    public interface IPermissionsAPI
    {
        #region Permissions

        IQueryable<Permission> Get(User user);
        ValueTask Add(User user, Permission value);
        ValueTask Update(User user, Permission value);
        ValueTask Delete(User user, Permission value);

        #endregion Permissions
    }
}
