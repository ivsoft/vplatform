using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Users.Api
{
    public interface IUserConfirmTokensAPI
    {
        #region UserConfirmTokens

        IQueryable<UserConfirmToken> Get(User user);
        ValueTask Add(User user, UserConfirmToken value);
        ValueTask Update(User user, UserConfirmToken value);
        ValueTask Delete(User user, UserConfirmToken value);

        #endregion UserConfirmTokens
    }
}
