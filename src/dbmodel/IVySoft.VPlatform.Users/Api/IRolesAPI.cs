using IVySoft.VPlatform.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Users.Api
{
    public interface IRolesAPI
    {
        #region Roles

        IQueryable<Role> Get(User user);
        ValueTask Add(User user, Role value);
        ValueTask Update(User user, Role value);
        ValueTask Delete(User user, Role value);

        #endregion Roles
        #region Permissions

        IQueryable<RolePermission> GetPermissions(User user, int Roles_key);
        ValueTask AddToPermissions(User user, int Roles_key, RolePermission value);
        ValueTask DeletePermissions(User user, int Roles_key, int key, RolePermission value);

        #endregion Permissions
    }
}
