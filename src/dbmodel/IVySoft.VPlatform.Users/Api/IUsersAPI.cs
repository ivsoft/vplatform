using IVySoft.VPlatform.Model;
using IVySoft.VPlatform.Users.Config;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Users.Api
{
    public interface IUsersAPI
    {
        #region Users

        IQueryable<User> Get();
        ValueTask Add(User value);
        ValueTask Update(User value);
        ValueTask Delete(User value);

        #endregion Users
        #region Tokens

        IQueryable<UserConfirmToken> GetTokens(int Users_key);
        ValueTask AddToTokens(int Users_key, UserConfirmToken value);
        ValueTask DeleteTokens(int Users_key, int key, UserConfirmToken value);

        #endregion Tokens

        #region Actions
        ValueTask<bool> Register([FromServices] IOptions<ServerConfig> configuration, string email);
        Task<string> Confirm([FromServices] IOptions<JwtConfig> configuration, string token, string password);
        ValueTask<string> RefreshToken([FromServices] IOptions<JwtConfig> configuration, [FromServices] User user);
        ValueTask<string> Login([FromServices] IOptions<JwtConfig> configuration, string email, string password);

        #endregion Actions
    }
}
