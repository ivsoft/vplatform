﻿using IVySoft.VPlatform.Security;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IVySoft.VPlatform.Users
{
    public class Role
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public string ObjectType { get; set; }

        [InverseProperty(nameof(RolePermission.Role))]
        public virtual IList<RolePermission> Permissions { get; set; }
    }
}
