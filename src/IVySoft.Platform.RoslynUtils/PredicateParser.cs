// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Linq.Expressions;
using Microsoft.CodeAnalysis.CSharp;

namespace IVySoft.Platform.RoslynUtils;
public static class PredicateParser
{
    public static Expression<Func<TEntity, bool>> Parse<TEntity>(string filter)
    {
        var tree = SyntaxFactory.ParseExpression(filter);
        var parameter = Expression.Parameter(typeof(TEntity), "x");

        return Expression.Lambda<Func<TEntity, bool>>(
            tree.Accept(new ParseLambdaBody<TEntity>(parameter)) ?? throw new Exception(""),
            parameter);
    }
}
