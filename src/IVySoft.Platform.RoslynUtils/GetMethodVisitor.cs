// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Linq.Expressions;
using System.Reflection;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace IVySoft.Platform.RoslynUtils;
internal class GetMethodVisitor<TEntity> : CSharpSyntaxVisitor<(Expression, string)>
{
    private readonly ParseLambdaBody<TEntity> _parseLambdaBody;

    public GetMethodVisitor(ParseLambdaBody<TEntity> parseLambdaBody)
    {
        _parseLambdaBody = parseLambdaBody;
    }
    public override (Expression, string) DefaultVisit(SyntaxNode node)
    {
        throw new Exception($"Unexpected node {node}");
    }
    public override (Expression, string) VisitMemberAccessExpression(MemberAccessExpressionSyntax node)
    {
        var result = node.Expression.Accept(_parseLambdaBody)!;
        return (result, node.Name.Identifier.ValueText);
    }
}
