// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Linq.Expressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace IVySoft.Platform.RoslynUtils;
internal class ParseLambdaBody<TEntity> : CSharpSyntaxVisitor<Expression>
{
    private readonly ParameterExpression _parameter;

    public ParseLambdaBody(ParameterExpression parameter)
    {
        _parameter = parameter;
    }

    public override Expression? DefaultVisit(SyntaxNode node)
    {
        return base.DefaultVisit(node);
    }
    public override Expression? VisitBinaryExpression(BinaryExpressionSyntax node)
    {
#pragma warning disable IDE0072 // Add missing cases
        return node.OperatorToken.Kind() switch
        {
            SyntaxKind.GreaterThanToken => Expression.GreaterThan(node.Left.Accept(this)!, node.Right.Accept(this)!),
            SyntaxKind.EqualsEqualsToken => Expression.Equal(node.Left.Accept(this)!, node.Right.Accept(this)!),
            SyntaxKind.AmpersandAmpersandToken => Expression.AndAlso(node.Left.Accept(this)!, node.Right.Accept(this)!),
            _ => throw new Exception($"Invalid node {node}"),
        };
#pragma warning restore IDE0072 // Add missing cases
    }

    public override Expression? VisitIdentifierName(IdentifierNameSyntax node)
    {
        return Expression.PropertyOrField(_parameter, node.Identifier.ValueText);
    }

    public override Expression? VisitMemberAccessExpression(MemberAccessExpressionSyntax node)
    {
        var result = node.Expression.Accept(this)!;
        var member = result.Type.GetProperty(node.Name.Identifier.ValueText);
        if(null == member)
        {
            throw new Exception($"Can't find member {node.Name.Identifier.ValueText} in the {result.Type.FullName}");
        }
        return Expression.MakeMemberAccess(result, member);
    }
    public override Expression? VisitInvocationExpression(InvocationExpressionSyntax node)
    {
        var arguments = node.ArgumentList.Arguments.Select(x => x.Accept(this)!).ToArray();
        var (instance, method) = node.Expression.Accept(new GetMethodVisitor<TEntity>(this));
        return Expression.Call(instance, method, null, arguments);
    }

    public override Expression? VisitLiteralExpression(LiteralExpressionSyntax node)
    {
#pragma warning disable IDE0072 // Add missing cases
        return node.Token.Kind() switch
        {
            SyntaxKind.NumericLiteralToken => Expression.Constant(int.Parse(node.Token.ValueText)),
            SyntaxKind.StringLiteralToken => Expression.Constant(node.Token.ValueText),
            _ => throw new Exception($"Invalid node {node}"),
        };
#pragma warning restore IDE0072 // Add missing cases
    }
}
