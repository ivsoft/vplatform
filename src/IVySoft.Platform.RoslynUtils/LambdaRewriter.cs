// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace IVySoft.Platform.RoslynUtils;
internal class LambdaRewriter : CSharpSyntaxRewriter
{
    public override SyntaxNode? DefaultVisit(SyntaxNode node)
    {
        throw new Exception($"Unexpected node {node}");
    }
}
